package br.senai.sp.informatica.contatos.util;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by Tecnico_Manha on 20/03/2017.
 */

public class MessageUtils {
    public static void toast(Context context, String mensagem){
        Toast.makeText(context, mensagem, Toast.LENGTH_SHORT).show();
    }
}
