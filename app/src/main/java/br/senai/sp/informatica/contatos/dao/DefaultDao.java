package br.senai.sp.informatica.contatos.dao;

import android.database.Cursor;

import java.util.List;

/**
 * Created by Tecnico_Manha on 20/03/2017.
 */
public interface DefaultDao<T> {
    public long save(T t);

    public List<T> findAll();

    public T fint(String name);

    public List<T> toList(Cursor cursor);
}
