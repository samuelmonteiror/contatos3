package br.senai.sp.informatica.contatos.activity;

import android.app.DatePickerDialog;
import android.database.sqlite.SQLiteConstraintException;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import br.senai.sp.informatica.contatos.R;
import br.senai.sp.informatica.contatos.activity.BaseActicity;
import br.senai.sp.informatica.contatos.dao.ContactDao;
import br.senai.sp.informatica.contatos.model.Cartao;
import br.senai.sp.informatica.contatos.model.Contact;
import br.senai.sp.informatica.contatos.util.MessageUtils;

public class MainActivity extends BaseActicity {

    //Lista de Cartoes

    List<String> creditCards = new ArrayList<>();

    Spinner creditCardSpinner;

    ImageView imageCartao;

    Button lastCallButton;

    int dia, mes ,ano;

    DatePickerDialog datepickerDialog;

    Locale localizacao = new Locale("pt", "Br");

    int [] bandeiras = new int[]{
            R.drawable.diners,
            R.drawable.visa,
            R.drawable.master,
            R.drawable.american_express,
            R.drawable.elo,
            R.drawable.hiper

    };

    EditText editNome;
    EditText editEmail;
    EditText editTelefone;
    RadioButton radioMasc;
    RadioButton radioFem;
    CheckBox checkClient;
    Contact contact;
    ContactDao contactDao;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        inicializarComponentes();
        loadCardList();
        loadCardSpinner();
        setLastCallButton();
    }

    @Override
    protected void inicializarComponentes() {

        creditCardSpinner = (Spinner) findViewById(R.id.spinner_cartao);
        creditCardSpinner.setOnItemSelectedListener(onCardSelect());
        imageCartao = (ImageView) findViewById(R.id.imagen_cartao);
        lastCallButton = (Button) findViewById(R.id.buttonLastCall);
        editNome=(EditText)findViewById(R.id.editNome);
        editEmail=(EditText)findViewById(R.id.editEmail);
        editTelefone=(EditText)findViewById(R.id.editTelefone);
        radioMasc=(RadioButton) findViewById(R.id.radioMasc);
        radioFem=(RadioButton) findViewById(R.id.radioFem);
        checkClient=(CheckBox) findViewById(R.id.check_cliente);

    }

    private AdapterView.OnItemSelectedListener onCardSelect(){
        return new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                imageCartao.setImageResource(bandeiras[position]);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        };
    }

    private void loadCardList() {
        for (Cartao c : Cartao.values()) {
            creditCards.add(c.bandeira);
        }
    }

    private void loadCardSpinner(){
        ArrayAdapter<String> adaptador = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, creditCards );
        creditCardSpinner.setAdapter(adaptador);
    }

    private void setLastCallButton(){
        Calendar calendario = Calendar.getInstance();

        ano = calendario.get(Calendar.YEAR);
        dia = calendario.get(Calendar.DAY_OF_MONTH);
        mes = calendario.get(Calendar.MONTH);

        lastCallButton.setText(String.format(localizacao, "%02d/%02d/%04d", dia, mes+1, ano));

        datepickerDialog = new DatePickerDialog(this, datePickerListener(), ano, mes, dia);


    }

    private DatePickerDialog.OnDateSetListener datePickerListener(){
           return new DatePickerDialog.OnDateSetListener() {
               @Override
               public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                   dia = dayOfMonth;
                   mes = month;
                   ano = year;

                   lastCallButton.setText(String.format(localizacao, "%02d/%02d/%04d", dia, mes+1, ano));
               }
           };



    }

    public void lastCallOnClick(View view){
        datepickerDialog.show();
    }

    public void SaveOnclick(View view) {
    }

    public void contactSaveOnclick(View view) {
        try {
            if (contact == null) {
                contact = new Contact();
            }
            validate();
            contact.setName(editNome.getEditableText().toString());
            contact.setEmail(editEmail.getEditableText().toString());
            contact.setTelefone(editTelefone.getEditableText().toString());
            contact.setCreditCard(creditCardSpinner.getSelectedItem().toString());
            if (radioFem.isSelected()) {
                contact.setSexo("Feminino");
            } else {
                contact.setSexo("Masculino");
            }
            Calendar lastCall = Calendar.getInstance();
            lastCall.set(ano, mes, dia);
            contact.setLastCall(lastCall);
            if (checkClient.isSelected()) {
                contact.setClient(true);
            } else {
                contact.setClient(false);
            }
            contactDao = new ContactDao(this);
            if (contactDao.save(contact) != -1) {
                MessageUtils.toast(this, getString(R.string.sucessoSalvar));
                resetForm();
            } else {
                MessageUtils.toast(this, getString(R.string.erroSalvar));
            }
        }
        catch(SQLiteConstraintException error){
            Log.e("Erro ao salvar: ", error.getMessage());
            MessageUtils.toast(this,getString(R.string.errorEmailIgual));
            Toast.makeText(this, R.string.errorEmailIgual, Toast.LENGTH_SHORT).show();
        }
    }
    private boolean validate(){
        if(editNome.getEditableText().toString().trim().isEmpty()){
            MessageUtils.toast(this, getString(R.string.nomeValidcao));
            editNome.requestFocus();
            return false;
        }else{
            if(editEmail.getEditableText().toString().trim().isEmpty()){
                MessageUtils.toast(this, getString(R.string.emailValidacao));
                editEmail.requestFocus();
                return false;
            }else{
                if(editTelefone.getEditableText().toString().trim().isEmpty()){
                    MessageUtils.toast(this, getString(R.string.telefoneValidacao));
                    editTelefone.requestFocus();
                    return false;
                }
            }
        }
        return true;
    }
    private void resetForm(){
        editNome.setText("");
        editEmail.setText("");
        editTelefone.setText("");
        radioMasc.setChecked(true);
        loadCardList();
        loadCardSpinner();
        setLastCallButton();
        checkClient.setChecked(false);
        editNome.requestFocus();
    }
}
