package br.senai.sp.informatica.contatos.model;

import java.util.Calendar;

/**
 * Created by Tecnico_Manha on 20/03/2017.
 */

public class Contact {
    private long id;
    private String name;
    private String email;
    private String telefone;
    private String creditCard;
    private String sexo;
    private Calendar lastCall;
    private boolean client;

    public Contact() {

    }

    public Contact(long id, String name, String email, String telefone, String creditCard, String sexo, Calendar lastCall, boolean client) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.telefone = telefone;
        this.creditCard = creditCard;
        this.sexo = sexo;
        this.lastCall = lastCall;
        this.client = client;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getCreditCard() {
        return creditCard;
    }

    public void setCreditCard(String creditCard) {
        this.creditCard = creditCard;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public Calendar getLastCall() {
        return lastCall;
    }

    public void setLastCall(Calendar lastCall) {
        this.lastCall = lastCall;
    }

    public boolean isClient() {
        return client;
    }

    public void setClient(boolean client) {
        this.client = client;
    }

    @Override
    public String toString() {
        return this.name;
    }
}
