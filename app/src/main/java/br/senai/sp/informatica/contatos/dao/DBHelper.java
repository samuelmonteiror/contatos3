package br.senai.sp.informatica.contatos.dao;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by Tecnico_Manha on 20/03/2017.
 */
public class DBHelper extends SQLiteOpenHelper {
    // Nome do banco de dados (schema)
    public static final String DB_NAME="contact.db";
    public static final int DB_VERSION=1;
    public static final String TAG="CONTACT_APP";
    public DBHelper(Context context) {
        /*
        context=contexto de execução
        DB_NAME=nome do banco de dados
        null=CursorFactory
        DB_VERSION=versão do banco
         */
        super(context, DB_NAME, null, DB_VERSION);
    }

    //String que representa os comandos SQL

    public static final String SQL_CONTACT="create table if not exists contact(" +
            "contactId INTEGER PRIMARY KEY AUTOINCREMENT," +
            "name TEXT NOT NULL," +
            "email TEXT NOT NULL UNIQUE COLLATE NOCASE," +
            "telefone TEXT NOT NULL," +
            "creditCard TEXT NOT NULL," +
            "sexo TEXT NOT NULL," +
            "lastCall NUMERIC NOT NULL," +
            "client NUMERIC NOT NULL" +
            ");";

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CONTACT);
        Log.e(TAG,"Crinado schemas e tabelas.");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
