package br.senai.sp.informatica.contatos.dao;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import br.senai.sp.informatica.contatos.model.Contact;
/**
 * Created by Tecnico_Manha on 20/03/2017.
 */
public class ContactDao implements DefaultDao<Contact> {

    private SQLiteDatabase sqLiteDatabase;

    private DBHelper dbHelper;

    private Context context;

    public ContactDao(Context context) {
        this.context = context;
        this.dbHelper=new DBHelper(context);
    }

    @Override
    public long save(Contact contact) {
        if(sqLiteDatabase==null){
           sqLiteDatabase=dbHelper.getWritableDatabase();
        }
        try{
            ContentValues values=new ContentValues();
            values.put("name",contact.getName());
            values.put("email",contact.getEmail());
            values.put("telefone",contact.getTelefone());
            values.put("creditCard",contact.getCreditCard());
            values.put("sexo",contact.getSexo());
            values.put("lastCall",contact.getLastCall().getTimeInMillis());
            values.put("client",contact.isClient());
            return sqLiteDatabase.insertOrThrow("contact","",values);
        }
        /*catch(SQLException error){

        }
        */
        finally {
            sqLiteDatabase.close();
        }
    }

    @Override
    public List<Contact> findAll() {
        //verifica se o arquivo de banco
        // de dados esta aberto
      if (sqLiteDatabase == null){
          //abre o arquivo
          sqLiteDatabase = dbHelper.getWritableDatabase();
      }
        try {
    //faz um select em todos os registros da tabela
            //contact e repassa as infromcacoes a um cursor
            String sql = "SELECT * FROM contact";
            Cursor cursor = sqLiteDatabase.rawQuery(sql, null);
            return toList(cursor);
        }finally {
            //fecha o arquivo de banco de dados
            sqLiteDatabase.close();
        }
    }

    @Override
    public Contact fint(String name) {
        return null;
    }

    @Override
    public List<Contact> toList(Cursor cursor) {
        List<Contact> contactsList = new ArrayList<>();

        //verifica se ha registro na tabela
        if(cursor.moveToFirst()){
            //percorre os registros
            //ate terminar e monta a lista de
            //contatos
            do{
                // cria um instancia de contato
                Contact contact = new Contact();
                // define os dados do contato
                contact.setId(cursor.getLong(cursor.getColumnIndex("contactID")));
                contact.setName(cursor.getString(cursor.getColumnIndex("name")));
                contact.setEmail(cursor.getString(cursor.getColumnIndex("email")));
                contact.setCreditCard(cursor.getString(cursor.getColumnIndex("creditCard")));
                contact.setSexo(cursor.getString(cursor.getColumnIndex("sexo")));
                //ultima chanmada
                Calendar calendario = Calendar.getInstance();
                //cria  uma data com os dados da tabela
                Date data = new Date (cursor.getLong(cursor.getColumnIndex("lastCall")));
                calendario.setTime(data);
                contact.setLastCall(calendario);
                if (cursor.getInt(cursor.getColumnIndex("client"))> 0){
                    contact.setClient(true);
                }else{
                    contact.setClient(false);
                }
            }while (cursor.moveToNext());
        }
        return
    }
}
