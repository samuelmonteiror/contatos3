package br.senai.sp.informatica.contatos.model;

/**
 * Created by Tecnico_Manha on 14/03/2017.
 */

public enum Cartao {

    DINNERS("Dinners Club"), VISA("Visa"), MASTER("Master Card"),AE("American Express"), ELO("Elo"), HIPER("Hiper Card");

    public String bandeira;

    Cartao(String bandeira) {
        this.bandeira = bandeira;
    }
}
